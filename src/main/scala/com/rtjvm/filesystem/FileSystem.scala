package com.rtjvm.filesystem

import java.util.Scanner

import com.rtjvm.commands.Command
import com.rtjvm.files.Directory

object FileSystem extends App {

  val  root = Directory.ROOT
  var state = State(root, root)  // For stateful applications
  val scanner = new Scanner(System.in)

  while (true) {

    state.show
    val input = scanner.nextLine()
    state = Command.from(input).apply(state)
  }
}
