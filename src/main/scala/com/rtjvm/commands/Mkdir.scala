package com.rtjvm.commands
import com.rtjvm.files.{DirEntry, Directory}
import com.rtjvm.filesystem.State

class Mkdir(name: String) extends CreateEntry(name) {

  override def createSpecificEntry(state: State): DirEntry =
    Directory.empty(state.wd.path, name)
}
