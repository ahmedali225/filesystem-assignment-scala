package com.rtjvm.commands
import com.rtjvm.files.{DirEntry, File}
import com.rtjvm.filesystem.State

class Touch(name: String) extends CreateEntry(name) {

  override def createSpecificEntry(state: State): DirEntry =
    File.empty(state.wd.path, name)
}
